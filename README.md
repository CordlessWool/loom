# Loom 2.0

> A simple blog theme based on [Gridsome Blog Starter](https://github.com/gridsome/gridsome-starter-blog)

## Developed for my blog

https://storyloom.de

## Install

### 1. Install Gridsome CLI tool if you don't have

`npm install --global @gridsome/cli`

### 2. Install this starter

1. `gridsome create my-gridsome-blog https://gitlab.com/CordlessWool/loom`
2. `cd my-gridsome-blog` to open folder
3. `echo GHOST_CONTENT_KEY=<your_ghost_content_key> > .env` to connect ghost API
4. `gridsome develop` to start local dev server at `http://localhost:8080`
5. Happy coding 🎉🙌

