// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here requires a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'Storyloom',
  siteDescription: 'Lyrische Ergüsse, gewebt aus dem Hirn der Wolle',
  outputDir: 'public',
  pathPregix: '/',
  templates: {
    GhostPost: '/:slug',
    GhostTag: '/tag/:slug',
    GhostAuthor: '/author/:slug',
    GhostPage: '/:slug'
  },

  plugins: [
    {
      use: '@gridsome/source-ghost',
      options: {
        baseUrl: 'https://loom.storyloom.de',
        contentKey: process.env.GHOST_CONTENT_KEY
      }
    }
  ],

  transformers: {
    //Add markdown support to all file-system sources
    remark: {
      externalLinksTarget: '_blank',
      externalLinksRel: ['nofollow', 'noopener', 'noreferrer'],
      anchorClassName: 'icon icon-link',
      plugins: [
        '@gridsome/remark-prismjs'
      ]
    }
  }
}
